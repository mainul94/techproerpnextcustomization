const set_last_selling_price = (frm, cdt, cdn)=>{
	let row = frappe.get_doc(cdt, cdn)
		if (!row.item_code || !frm.doc.customer)
			return;
		frappe.call({
			method: "tech_pro_erpnext_customization.utils.sales_invoice.get_last_selling_price",
			args: {
				customer: frm.doc.customer,
				item_code: row.item_code
			},
			callback: r=>{
				if(r['message']){
					console.log(r.message[1])
					row.last_selling_price = r.message[0]
					row.purchase_rate = r.message[1]
					frm.refresh_field('items')
				}
			}
		})
}


frappe.ui.form.on('Sales Invoice', {
	setup: frm => {
		frm.set_query('link_barcode', 'items', function(doc, cdt, cdn){
			let row = frappe.get_doc(cdt, cdn)
			return {
				filters: {
					"parent": row.item_code
				}
			}
		})
	},
	customer: frm =>{
		if (!frm.doc.items)
			return;
		frm.doc.items.forEach(row=>{
			set_last_selling_price(frm, row.doctype, row.name)
		})
	}
})


frappe.ui.form.on('Sales Invoice Item', {
	item_code: (frm, cdt,cdn) =>{
		set_last_selling_price(frm, cdt,cdn)
	}
})