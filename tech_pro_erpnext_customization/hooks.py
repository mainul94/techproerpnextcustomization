# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "tech_pro_erpnext_customization"
app_title = "ERPNext Customization"
app_publisher = "mainulkhan94@gmail.com"
app_description = "All Customization for TechPro"
app_icon = "octicon coticon-plug"
app_color = "green"
app_email = "mainulkhan94@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
app_include_css = "assets/css/tech_pro_erpnext_customization.css"
# app_include_js = "assets/js/tech_pro_erpnext_customization.js"

# include js, css files in header of web template
# web_include_css = "/assets/tech_pro_erpnext_customization/css/tech_pro_erpnext_customization.css"
# web_include_js = "/assets/tech_pro_erpnext_customization/js/tech_pro_erpnext_customization.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
doctype_js = {
	"Sales Invoice": "public/js/sales_invoice.js"
}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "tech_pro_erpnext_customization.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "tech_pro_erpnext_customization.install.before_install"
# after_install = "tech_pro_erpnext_customization.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "tech_pro_erpnext_customization.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"tech_pro_erpnext_customization.tasks.all"
# 	],
# 	"daily": [
# 		"tech_pro_erpnext_customization.tasks.daily"
# 	],
# 	"hourly": [
# 		"tech_pro_erpnext_customization.tasks.hourly"
# 	],
# 	"weekly": [
# 		"tech_pro_erpnext_customization.tasks.weekly"
# 	]
# 	"monthly": [
# 		"tech_pro_erpnext_customization.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "tech_pro_erpnext_customization.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.www.printview.get_html_and_style": "tech_pro_erpnext_customization.utils.printview.get_html_and_style"
# }

jenv = {
	"methods": "trigger_barcode_build:tech_pro_erpnext_customization.utils.printview.BARCODE_GENERATE"
}