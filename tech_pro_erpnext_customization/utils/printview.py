import frappe
from barcode import generate, PROVIDED_BARCODES
from frappe.utils import get_files_path, get_url
import os


def BARCODE_GENERATE (item_barcode, barcode_type=None):
	if not item_barcode:
		return ''
	if not barcode_type:
		barcode_type = 'code128'
	barcode_type = barcode_type.replace(' ', '').replace('-', '').lower()
	if barcode_type not in PROVIDED_BARCODES:
		frappe.msgprint("Doesn't Support Barcode Type '{}'".format(barcode_type))
	file_path = get_files_path("{}".format(item_barcode))
	uri = "files/{}.svg".format(item_barcode)
	if os.path.exists(file_path+'.svg'):
		return get_barcode_img(get_url(uri))
	generate(barcode_type, item_barcode, output=file_path, writer_options={"module_height": 10})
	return get_barcode_img(get_url(uri))

def get_barcode_img(url):
	return """
	<img src="{}" />
	""".format(url)