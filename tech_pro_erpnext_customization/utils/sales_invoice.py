import frappe
from frappe.desk.reportview import get_match_cond, get_filters_cond


@frappe.whitelist()
def get_last_selling_price(customer, item_code):
	sql = frappe.db.sql("""select rate from `tabSales Invoice Item` sii
	 right join `tabSales Invoice` si on si.name = sii.parent
	 where si.docstatus=1 and si.customer='{}' and sii.item_code='{}' order by posting_date desc limit 1""".format(customer, item_code))
	price = [0, 0]
	price[0] = sql[0][0] if len(sql) else 0
	last_p_rate = frappe.db.sql("""select rate from `tabPurchase Order Item`
	 right join `tabPurchase Order` on `tabPurchase Order`.name = `tabPurchase Order Item`.parent
	 where `tabPurchase Order`.docstatus=1 and item_code='{}' order by transaction_date desc limit 1""".format(item_code))
	price[1] = last_p_rate[0][0] if len(last_p_rate) else 0
	return price
