import frappe
from frappe.modules.utils import sync_customizations

def execute():
	sync_customizations('tech_pro_erpnext_customization')
	if 'barcodes' in frappe.db.get_table_columns("Sales Invoice Item"):
		frappe.db.sql("""update `tabSales Invoice Item` set link_barcode=barcodes where barcodes is not null""")
	frappe.delete_doc_if_exists('Custom Field', 'Sales Invoice Item-barcodes')
